select 
	cast((case when length(e.name) > 0 then e.name when e.username then e.username else e.id end) as text) as name,
	count(*) total_msgs, 
	count(case when (m.message like '%fuck%' or m.message like '%\Saf\S%') then 1 else null end) total_fucks,

	cast(
		printf("%.2f", (count(case when (m.message like '%fuck%' or m.message like '%\Saf\S%') then 1 else null end) / 
			(count(*) / cast(count(distinct date(m.timestamp)) as float)))
		) as float 
	) as fuck_score,

	min(date(m.timestamp)) as first_msg,
	cast((julianday('now') - julianday(min(date(m.timestamp)))) as int) as days_since_first_msg,
	
	count(distinct date(m.timestamp)) as days_contrib,
	cast(
		printf("%.2f", 
			count(*) / cast(count(distinct date(m.timestamp)) as float)
		) as float 
	) as msgs_per_days_contrib,

	cast(
		printf("%.2f", 
			count(distinct date(m.timestamp)) / 
			cast((julianday('now') - julianday(min(date(m.timestamp)))) as float)
		) as float
	)	as days_active_ratio, 
	cast(
		printf("%.2f", 
			count(distinct date(m.timestamp)) / 
			cast((julianday('now') - julianday(min(date(m.timestamp)))) as float) * 
			(count(*) / cast(count(distinct date(m.timestamp)) as float))
		) as float
	) as days_active_msgs_ratio
	
	
	
from message m 
join entity e 
	on m.entity_id=e.id 
join channel c 
	on m.channel_id=c.id 
where 
	-- c.username='JacksonvilleBitcoin' 
	-- c.username = 'FloridaBitcoin'
	-- c.id = 1275672231
	c.id = 1105208409 -- eth+
	-- c.username = 'bitcorns'
group by 
	e.id 
order by 
	days_active_msgs_ratio desc
	-- total_fucks_mpdc desc
limit 20