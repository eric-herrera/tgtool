# TG Tool

Download telegram channels, users, messages, media

## Setup

```
$ git clone https://gitlab.com/eric-herrera/tgtool
$ virtualenv -p python3 venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```

## Authorization (first time)

```
(venv) $ ./tgtool list dialogs
Please enter your phone: 1NPANXXxxxx
Please enter the code you received: 12345
Signed in successfully as Eric Herrera
Connected as Eric Herrera
..
```

## Add channel to local db

```
(venv) $ ./tgtool add [channel_username]
```

## Download all messages and entities in channel

```
(venv) $ ./tgtool sync [channel_username] -t all
```
## Querying


```
(venv) $ ./tgtool list channels 
```

```
(venv) $ ./tgtool list entities
```

```
(venv) $ ./tgtool list messages
```