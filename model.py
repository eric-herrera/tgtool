from datetime import datetime

from sqlalchemy import (
	create_engine, 
	Column, Integer, BigInteger, Boolean, String, Text, DateTime, Float, 
	ForeignKey, UniqueConstraint, ForeignKeyConstraint, PrimaryKeyConstraint, 
	and_
)
from sqlalchemy.orm import relationship, Session, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property


from sqlalchemy.ext.compiler import compiles
from sqlalchemy.dialects.postgresql.json import JSONB

@compiles(JSONB, "sqlite")
def compile_binary_sqlite(type_, compiler, **kw):
    return "TEXT"


Base = declarative_base()

class Channel(Base):
	__tablename__ = 'channel'

	id = Column(Integer, primary_key=True)
	username = Column(String(32), nullable=True)
	name = Column(String(32), nullable=True)
	about = Column(Text(), nullable=True)
	photo_id = Column(BigInteger(), nullable=True)
	got_photo = Column(Boolean(), nullable=False, default=False)
	
	messages = relationship("Message", cascade="all, delete-orphan", backref='channel') # , lazy='joined'
	entities = relationship("Entity", secondary="channel_entity", viewonly=True)
	
	def __init__(self, id):
		self.id = id

class Entity(Base):
	__tablename__ = 'entity'

	id = Column(Integer, primary_key=True)
	username = Column(String(32), nullable=True)
	first_name = Column(String(32), nullable=False, default='')
	last_name = Column(String(32), nullable=False, default='')
	about = Column(Text(), nullable=True)
	photo_id = Column(BigInteger(), nullable=True)
	got_photo = Column(Boolean(), nullable=False, default=False)
	messages = relationship("Message", backref='entity') # , lazy='joined'

	channels = relationship("Channel", secondary="channel_entity", viewonly=True)

	# Computed
	first_timestamp = Column(DateTime, nullable=True)
	qty = Column(Integer)

	def __init__(self, id):
		self.id = id

	@hybrid_property
	def name(self):
		return self.first_name + ' ' + self.last_name


class Message(Base):
	__tablename__ = 'message'

	pk = Column(Integer, primary_key=True)
	id = Column(Integer)
	timestamp = Column(DateTime, nullable=False, default=datetime.now())
	message = Column(Text())
	reply_to_msg_id = Column(Integer(), nullable=True)

	entity_id = Column(Integer, ForeignKey('entity.id'))
	channel_id = Column(Integer, ForeignKey('channel.id'))

	__table_args__ = (
		UniqueConstraint('channel_id', 'id', name='uix_1'),
	)

	def __init__(self, id, timestamp, message, channel_id, entity_id):
		self.id = id
		self.timestamp = timestamp
		self.message = message
		self.channel_id = channel_id
		self.entity_id = entity_id

class MessageDoc(Base):
	__tablename__ = 'message_doc'

	#pk = Column(Integer, primary_key=True)
	channel_id = Column(Integer, nullable=False)
	message_id = Column(Integer, nullable=False)
	doc = Column(JSONB)

	#__table_args__ = (
	#	UniqueConstraint('channel_id', 'message_id', name='uix_mc_cid_mid'),
	#)

	
	__table_args__ = (
		PrimaryKeyConstraint('channel_id', 'message_id', name='message_cache_pk'),
	)

	"""
	def __init__(self, channel_id, message_id, data):
		self.channel_id = channel_id
		self.message_id = message_id
		self.data = data
	"""

class ChannelEntity(Base):
	__tablename__ = 'channel_entity'

	pk = Column(Integer, primary_key=True)
	
	channel_id = Column(Integer, ForeignKey('channel.id'))
	entity_id = Column(Integer, ForeignKey('entity.id'))

	channel = relationship("Channel", backref=backref("channel_entity"))
	entity = relationship("Entity", backref=backref("channel_entity"))

	# Computed
	first_timestamp = Column(DateTime, nullable=True)
	qty = Column(Integer)

	__table_args__ = (
		UniqueConstraint('channel_id', 'entity_id', name='uix_channel_entity'),
	)

	def __init__(self, channel_id, entity_id):
		self.channel_id = channel_id
		self.entity_id = entity_id


class MessageSyncCursor(Base):
	__tablename__ = 'message_sync_cursor'

	channel_id = Column(Integer, primary_key=True)
	min_id = Column(Integer)
	offset = Column(Integer)
	
	def __init__(self, channel_id):
		self.channel_id = channel_id
		